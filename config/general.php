<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 */

return [
    '*' => [
        'defaultWeekStartDay' => 0,
        'defaultImageQuality' => 85,
        'maxUploadFileSize' => 16777216,
        'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
        'securityKey' => getenv('SECURITY_KEY'),
    ],
    'localhost' => [
        'devMode' => true,
        'siteUrl' => 'http://localhost:3000',
    ],
    'dev' => [
        'devMode' => true,
        'siteUrl' => 'http://craft3-react.dev',
    ],
    'staging' => [
        'siteUrl' => null,
    ],
    'production' => [
        'siteUrl' => null,
    ],
];
