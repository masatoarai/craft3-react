const mix = require('laravel-mix');
const globImporter = require('node-sass-glob-importer');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix
  .webpackConfig({
    devtool: 'inline-source-map',
    output: { chunkFilename: 'scripts/[name][chunkhash].js' },
    module: {
      rules: [
        {
          test: /\.s[ac]ss$/,
          use: [
            {
              loader: 'sass-loader',
              options: {
                importer: globImporter(),
              },
            },
          ],
        },
      ],
    },
  })
  .setPublicPath('web')
  .setResourceRoot('resources')
  .js('resources/scripts/main.js', 'web/scripts')
  .extract([
    'domready',
    'fastclick',
    'jquery',
    'lodash',
    'wolfy87-eventemitter',
  ])
  .sass('resources/styles/main.scss', 'web/styles');

if (mix.inProduction) {
  mix.sourceMaps(false);
  mix.version();
} else {
  mix.sourceMaps();
}

mix.browserSync({
  proxy: 'craft3-react.dev',
  files: [
    'templates/**/*.twig',
    'web/styles/**/*.css',
    'web/scripts/**/*.js',
  ],
});
